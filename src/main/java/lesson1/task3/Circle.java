package lesson1.task3;

import java.util.Scanner;

public class Circle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the radius of the circle\nR = ");
        int r = scanner.nextInt();
        double с = 2 * Math.PI * r;
        System.out.println("Circumference = " + с);
    }
}
