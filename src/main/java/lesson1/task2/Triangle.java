package lesson1.task2;

import java.util.Scanner;

public class Triangle {
    public static void main(String[] args) {
        System.out.println("Enter the length of the sides of the triangle");
        Scanner scanner = new Scanner(System.in);
        System.out.print("a = ");
        int a = scanner.nextInt();
        System.out.print("b = ");
        int b = scanner.nextInt();
        System.out.print("c = ");
        int c = scanner.nextInt();
        if (a + b <= c | a + c <= b | b + c <= a) {
            System.out.println("There is no triangle with such sides");
        } else {
            double p = ((double) a + b + c) / 2;
            double inter = p * (p - a) * (p - b) * (p - c);
            double s = Math.sqrt(inter);
            System.out.println("The area of the triangle is " + s);
        }
    }
}
