package Viete;

import java.util.Scanner;

public class VieteTheorem {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("x²+bx+c=0");

        System.out.print("Enter b ");
        int b = scanner.nextInt();
        System.out.print("Enter c ");
        int c = scanner.nextInt();

        compute(b, c);


    }

    private static void compute(int b, int c) {
        int cm = Math.abs(c);
        int bm = Math.abs(b);
        int x1 = 0;
        int x2 = 0;

        for (int i = -(bm * cm); i < (bm * cm); i++) {
            for (int j = -(cm + bm); j < (cm + bm); j++) {
                if ((c == i * j) && (-b == i + j)) {
                    x1 = i;
                    x2 = j;
                }
            }
        }
        if (x1 == 0 && x2 == 0)
            System.out.println("Such an equation is difficult to find with the help of the Viete theorem.");
        else System.out.println("x1 = " + x1 + ", " + "x2 = " + x2);

    }
}
