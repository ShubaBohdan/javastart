package lesson3.task2;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        System.out.print("Enter a number in the range 4 < n <16\nn = ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n>4 && n<16){
            int s = n;
            for (int i = n-1; i > 0; i--) {
                s *= i;
            }
            System.out.println(n+"! = "+ s);
        }else System.out.println("The entered number does not meet the condition");
    }
}
