package lesson3.task6;

public class PrimeNumber {
    public static void main(String[] args) {
        int counter=0;
        for (int i = 1; i <= 100; i++) {
            for (int j = 1; j < i; j++) {
                if (i % j == 0) counter++;
            }
            if (counter == 1) System.out.println(i);
            counter = 0;
        }
    }
}
