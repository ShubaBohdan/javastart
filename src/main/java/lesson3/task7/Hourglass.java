package lesson3.task7;

import java.util.Scanner;

public class Hourglass {
    public static void main(String[] args) {
        System.out.println("Enter an odd number: ");
        Scanner scanner = new Scanner(System.in);
        int w = scanner.nextInt();

        if (w % 2 != 0) {
            int wN = 0;

            for (int i = 0; i <= w * 2 - 1; i += 2) {
                if (i < w) {
                    System.out.print(new String(new char[(i - wN++)]).replace('\0', ' ') + new String(new char[(w - i)]).replace('\0', '*') + "\n");
                } else {
                    System.out.print(new String(new char[(wN-- - 2)]).replace('\0', ' ') + new String(new char[(i - w + 2)]).replace('\0', '*') + "\n");
                }
            }
        }else System.out.println("This is not an odd number!");
    }
}