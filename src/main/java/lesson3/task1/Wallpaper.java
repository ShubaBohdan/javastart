package lesson3.task1;

import java.util.Scanner;

public class Wallpaper {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the number of lanes: ");
        int num = scanner.nextInt();
        System.out.print("Enter the number of rows: ");
        int col = scanner.nextInt();
        for (int j = 0; j < col; col--) {
            int cek = num;
            for (int i = 0; i < cek; cek--) {
                if (cek % 2 == 0) System.out.print("+++");
                else System.out.print("***");
            }
            System.out.println();
        }
    }
}
