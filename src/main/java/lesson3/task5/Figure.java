package lesson3.task5;

import java.util.Scanner;

public class Figure {
    public static void main(String[] args) {
        System.out.print("Figure height: ");
        Scanner scanner = new Scanner(System.in);
        int h = scanner.nextInt();
        String sb = new String("");
        for (int i = 0; i < 2*h - 1; i++) {
            if (i < h) System.out.println(sb += "*");
            else  System.out.println(sb.substring(i - h + 1));
        }
    }
}