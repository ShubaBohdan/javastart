package lesson3.task4;

import java.util.Scanner;

public class Rectangle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Rectangle height: ");
        int h = scanner.nextInt();
        System.out.print("Rectangle width: ");
        int w = scanner.nextInt();
        char[][] mat = new char[h][w];
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                if (i == 0 || j == 0 || i == h - 1 || j == w - 1) {
                    mat[i][j] = '*';
                } else mat[i][j] = ' ';
            }
        }
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                System.out.print(mat[i][j]);
            }
            System.out.println();
        }

    }
}
