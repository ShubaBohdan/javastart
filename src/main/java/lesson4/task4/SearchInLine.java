package lesson4.task4;

import java.util.Scanner;

public class SearchInLine {
    public static void main(String[] args) {
        int count = 0;

        System.out.print("Enter the string: ");
        Scanner scanner = new Scanner(System.in);

        String str = scanner.nextLine();
        char[] str1 = str.toCharArray();

        for (int i = 0; i < str1.length; i++) {
            if (str1[i] == 'b') count++;
        }

        System.out.println("The number of characters 'b': " + count);
    }
}
