package lesson4.task1;

public class Array {
    public static void main(String[] args) {

        int[] array = {0, 5, 2, 4, 7, 1, 3, 19};
        int counter = 0;

        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] % 2 != 0) counter++;
        }
        System.out.print(counter);
    }
}
