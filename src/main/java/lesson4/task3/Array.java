package lesson4.task3;

import java.util.Arrays;

public class Array {
    public static void main(String[] args) {

        int[] array1 = new int[15];

        for (int i = 0; i < array1.length; i++) {
            array1[i] = 1 + (int) (Math.random() * 10);
        }

        int[] array2 = Arrays.copyOf(array1, array1.length * 2);

        for (int i = array2.length / 2; i < array2.length; i++) {
            array2[i] = array1[i - array1.length] * 2;
        }

        System.out.println("Array array1: " + Arrays.toString(array1));
        System.out.println("Array array2: " + Arrays.toString(array2));
    }
}
