package lesson4.task2;

import java.util.Arrays;
import java.util.Scanner;

public class Array {
    public static void main(String[] args) {

        System.out.print("Enter the number of elements in the array: ");
        Scanner scanner = new Scanner(System.in);
        int ar = scanner.nextInt();

        int[] array = new int[ar];
        for (int i = 0; i < array.length; i++) {
            System.out.print("array[" + i + "] = ");
            array[i] = scanner.nextInt();
        }
        System.out.println("array: " + Arrays.toString(array));
    }
}