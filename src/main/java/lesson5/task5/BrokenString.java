package lesson5.task5;

import java.util.Scanner;

public class BrokenString {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter text");
        String s = scanner.nextLine();

        System.out.println("The number of words in line: " + beat(s));
    }

    private static int beat(String s) {
        String[] words = s.split(" ");
        return words.length;
    }
}
