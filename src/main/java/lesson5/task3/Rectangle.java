package lesson5.task3;

import java.util.Scanner;

public class Rectangle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Rectangle height ");
        int h = scanner.nextInt();
        System.out.print("Rectangle width ");
        int w = scanner.nextInt();
        rec(h, w);
    }

    private static void rec(int height, int width) {
        char[][] a = new char[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                a[i][j] = '*';
                if (j == width - 1) System.out.println(a[i][j]);
                else System.out.print(a[i][j]);
            }
        }
    }
}
