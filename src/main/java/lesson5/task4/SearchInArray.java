package lesson5.task4;

import java.util.Arrays;
import java.util.Scanner;

public class SearchInArray {
    public static void main(String[] args) {
        int[] array = new int[15];
        fillArray(array);
        System.out.println(Arrays.toString(array));

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number: ");
        int numb = scanner.nextInt();

        search(array, numb);
    }

    private static void search(int[] array, int numb) {
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == numb) {
                count++;
                System.out.println("Such an element is in the array, its index is " + i);
            }

        }if (count==0){
            System.out.println(-1);
        }
    }

    private static int[] fillArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 20) - 10;
        }
        return array;
    }
}
