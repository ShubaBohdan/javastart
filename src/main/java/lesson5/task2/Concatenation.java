package lesson5.task2;

public class Concatenation {
    public static void main(String[] args) {
        int i = 3;
        double d = 5.6;
        String s = "Sum of numbers:";

        System.out.println(sum(i, d, s));
    }

    private static String sum(int a, double b, String c) {
        return c + " " + (b + a);
    }
}
