package lesson5.task1;

public class Max {
    public static void main(String[] args) {
        int[] a = {7, 6, 3, 10, 5, 7, 8};

        System.out.println(sum(a));
    }

    private static int sum(int[] a) {
        int max = a[0];

        for (int i = 1; i < a.length; i++) {
            if (max < a[i]) max = a[i];
        }
        return max;
    }
}

