package lesson2.task4;

import java.util.Scanner;

public class Triangle {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();

        if (a + b <= c | a + c <= b | b + c <= a) {
            System.out.println("There is no triangle with such sides");

        } else {
            System.out.println("A triangle with such sides exists");
        }
    }
}
