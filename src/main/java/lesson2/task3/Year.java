package lesson2.task3;

import java.util.Scanner;

public class Year {
    public static void main(String[] args) {
        System.out.println("Enter year");
        Scanner scanner = new Scanner(System.in);
        int year = scanner.nextInt();

        if ((year % 4 == 0) & (year % 100 != 0) | (year % 400 == 0)) {
            System.out.println("This is a leap year. This year 366 days");
        } else {
            System.out.println("This year is not a leap year. This year 365 days");
        }
    }
}
