package lesson2.task7;

import java.util.Scanner;

public class LuckyNum {
    public static void main(String[] args) {
        System.out.println("Enter the number");
        Scanner numb = new Scanner(System.in);
        int number = numb.nextInt();
        String str = Integer.toString(number);
        char[] lucNum = str.toCharArray();
        if (lucNum[0] + lucNum[1] == lucNum[2] + lucNum[3])
            System.out.println("This lucky number");
        else System.out.println("You were unlucky(");
    }
}
