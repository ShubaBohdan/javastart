package lesson2.task8;

import java.util.Scanner;

public class Palindrome {
    public static void main(String[] args) {
        System.out.println("Enter the number");
        Scanner numb = new Scanner(System.in);
        char[] pol = String.valueOf(numb.nextInt()).toCharArray();
        for (int i = 0; i < pol.length / 2; i++) {
            if (pol[i] != pol[pol.length - i - 1]) {
                System.out.println("The number is not a palindrome");
                break;
            } else {
                System.out.println("The number is a palindrome");
                break;
            }
        }
    }
}
