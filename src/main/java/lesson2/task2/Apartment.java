import java.util.Scanner;

public class Apartment {
    public static void main(String[] args) {
        System.out.print("Enter the apartment number: ");

        Scanner num = new Scanner(System.in);
        int apart = num.nextInt();
        int roomsPerFloor = 4;                                                               //квартир на этаже
        int floorsPerDoorway = 9;                                                            // количество этажей
        int entranceBuilding = 4;                                                            // всего подъездов

        if (0 < apart && apart <= roomsPerFloor * floorsPerDoorway * entranceBuilding) {
            int entrance = apart / (roomsPerFloor * floorsPerDoorway);                                             // остача не сохраняется
            if (apart % (roomsPerFloor * floorsPerDoorway) > 0) {                                                  //если есть остача то +1
                entrance++;
            }
            System.out.println("Entrance: " + entrance);
            double abstractFloors = ((double) apart / roomsPerFloor);                        //этаж если бы дом был с одним подъездом)
            int floor = ((int) abstractFloors - ((entrance - 1) * floorsPerDoorway));        //остачу не сохраняем
            if (apart % roomsPerFloor > 0) {                                                 //если есть остача то +1
                floor++;
            }
            System.out.println("Floor: " + floor);
        } else {
            System.out.println("In this house there is no apartment with such a number");
        }
    }
}

        /* Старое решение задачи + добавлен метод
import java.util.Scanner;

public class Apartment {
    public static void main(String[] args) {
        System.out.print("Enter the apartment number: ");

        Scanner num = new Scanner(System.in);
        int apart = num.nextInt();
        int i = 4;
        int t = 0;

        if (1 <= apart & apart <= 36) {
            Floor(apart, 0);
        } else if (37 <= apart & apart <= 72) {
            apart -= 36;
            Floor(apart, 0);
        } else if (73 <= apart & apart <= 108) {
            apart -= 72;
            Floor(apart, 0);
        } else if (109 <= apart & apart <= 144) {
            apart -= 108;
            Floor(apart, 0);
        } else {
            System.out.println("In this house there is no apartment with such a number");
        }

    }

   /* public static void Floor(int a, int t) {

        while (a > 0) {
            a -= 4;
            ++t;
        }
        System.out.println("Entrance: 2" + ", floor: " + t);
    }

}
 */