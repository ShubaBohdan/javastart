import java.util.Scanner;

public class Numbers {
    public static void main(String[] args) {
        System.out.println("Enter numbers");
        Scanner num = new Scanner(System.in);
        int num1 = num.nextInt();
        int num2 = num.nextInt();
        int num3 = num.nextInt();
        int num4 = num.nextInt();
        if (num1 < num2) {
            num1 = num2;
        }
        if (num1 < num3) {
            num1 = num3;
        }
        if (num1 < num4) {
            System.out.println("Larger number: " + num4);
        } else {
            System.out.println("Larger number: " + num1);
        }
    }
}
