package lesson2.task5;

import java.util.Scanner;

public class Circle {
    public static void main(String[] args) {
        System.out.println("Enter the coordinates of points");
        Scanner sc = new Scanner(System.in);
        System.out.print("x = ");
        double x = sc.nextDouble();
        System.out.print("y = ");
        double y = sc.nextDouble();
        if (Math.pow(x, 2.0) + Math.pow(y, 2.0) <= 16) {
            System.out.println("The point lies in the circle");
        } else {
            System.out.println("The point does not lie in the circle");
        }
    }
}
