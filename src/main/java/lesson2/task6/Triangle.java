package lesson2.task6;

import java.util.Scanner;

public class Triangle {
    public static void main(String[] args) {

        int ax = 0;
        int ay = 0;
        int bx = 4;
        int by = 4;
        int cx = 6;
        int cy = 1;

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter coordinate x: ");
        int x = scanner.nextInt();
        System.out.print("Enter coordinate y: ");
        int y = scanner.nextInt();

        int a = (ax - x) * (by - ay) - (bx - ax) * (ay - y);
        int b = (bx - x) * (cy - by) - (cx - bx) * (by - y);
        int c = (cx - x) * (ay - cy) - (ax - cx) * (cy - y);

        if ((a >= 0 && b >= 0 && c >= 0) || (a <= 0 && b <= 0 && c <= 0)) {
            if (a == 0 || b == 0 || c == 0)
                System.out.println("The point lies on the side of the triangle");
            else System.out.println("The point belongs to the triangle");
        } else System.out.println("The point does not belong to the triangle");
    }
}
